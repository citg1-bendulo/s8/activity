-- a.find all artists with d in its name
SELECT * FROM artists WHERE name LIKE "%d%"
-- b.find all songs that has length of less than 230
SELECT * FROM songs WHERE length < 230
-- c.joing the albums and songs tables;
SELECT album_title, song_name,length FROM albums JOIN songs ON songs.album_id=albums.id;
-- d.join artista and albus tables
SELECT * FROM artists JOIN albums ON artists.id=albums.artist_id WHERE albums.album_title LIKE "%a%";
-- e. sort all albums z-a with 4 records
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;
-- f.join albums and songs
SELECT * from albums JOIN songs ON songs.album_id=albums.id order by albums.album_title DESC,songs.song_name ASC;